﻿using FEx.Basics.Extensions;
using FlakEssentials.MSBuild;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace DotNetProjectsToolkit.Core;

public class XamlFile : XamlEntry
{
    public string XamlContent { get; private set; }
    public List<XamlString> Strings { get; }

    public XamlFile(MSProjectItem xaml, MSProject project)
        : base(xaml.EvaluatedInclude, project)
    {
        Strings = [];
    }

    public async Task LoadXamlContentAsync(IList<string> tags, IList<string> excludes)
    {
        try
        {
            string xamlContent;

            using (var src = new FileStream(AbsolutePath, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                using (var sr = new StreamReader(src))
                    xamlContent = await sr.ReadToEndAsync();
            }

            using (var stringReader = new StringReader(xamlContent))
            using (var reader = XmlReader.Create(stringReader,
                       new()
                       {
                           Async = true
                       }))
            {
                Strings.Clear();

                while (await reader.ReadAsync())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element:
                            foreach (string tag in tags)
                            {
                                string curr = reader.GetAttribute(tag);

                                if (!string.IsNullOrEmpty(curr))
                                {
                                    var matched = false;

                                    foreach (string exclude in excludes)
                                    {
                                        var regex = new Regex(exclude);
                                        Match match = regex.Match(curr);

                                        if (match.Success)
                                            matched = true;
                                    }

                                    if (!matched)
                                        Strings.Add(new(this)
                                        {
                                            Tag = tag,
                                            Text = curr.Replace("\"", ""),
                                            LineNr = ((IXmlLineInfo)reader).LineNumber
                                        });
                                }
                            }

                            break;
                    }
                }
            }

            XamlContent = xamlContent;
        }
        catch (Exception ex)
        {
            ex.HandleException();
        }
    }
}