﻿using FEx.Basics.Abstractions;

namespace DotNetProjectsToolkit.Core;

public class XamlString : NotifyPropertyChanged
{
    public string Text { get; set; }
    public int LineNr { get; set; }
    public string Tag { get; set; }
    public bool Valid { get; set; }
    public string Key { get; set; }
    public XamlEntry XamlEntry { get; }

    public XamlString(XamlEntry file)
    {
        XamlEntry = file;
    }
}