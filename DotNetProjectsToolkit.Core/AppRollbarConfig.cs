﻿using FEx.Common.Abstractions.Interfaces;
using FEx.Telemetry.Subjects;
using FlakEssentials.RollbarEx;

namespace DotNetProjectsToolkit.Core;

public class AppRollbarConfig : FExRollbarConfig
{
    public AppRollbarConfig(TelemetryAccessTokenSubject telemetryAccessTokenSubject, IAppInfoProvider appInfoProvider)
        : base(telemetryAccessTokenSubject, appInfoProvider, "df0641560e084261842b60f341a89766")
    {
    }
}