﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DotNetProjectsToolkit.Core;

public static class MoveClassToFile
{
    public static async Task<Solution> RenameFileAsync(this Document document,
                                                       BaseTypeDeclarationSyntax typeDecl,
                                                       CancellationToken cancellationToken)
    {
        SyntaxToken identifierToken = typeDecl.Identifier;
        SyntaxTree currentSyntaxTree = await document.GetSyntaxTreeAsync(cancellationToken);
        SyntaxNode currentRoot = await currentSyntaxTree.GetRootAsync(cancellationToken);

        Project project = document.Project.RemoveDocument(document.Id);
        Document newDocument = project.AddDocument($"{identifierToken.Text}.cs", currentRoot, document.Folders);

        return newDocument.Project.Solution;
    }

    public static async Task<Solution> MoveClassIntoNewFileAsync(this Document document,
                                                                 BaseTypeDeclarationSyntax typeDecl,
                                                                 CancellationToken cancellationToken)
    {
        SyntaxToken identifierToken = typeDecl.Identifier;

        // symbol representing the type
        SemanticModel semanticModel = await document.GetSemanticModelAsync(cancellationToken);
        INamedTypeSymbol typeSymbol = semanticModel.GetDeclaredSymbol(typeDecl, cancellationToken);

        // remove type from current files
        SyntaxTree currentSyntaxTree = await document.GetSyntaxTreeAsync(cancellationToken);
        SyntaxNode currentRoot = await currentSyntaxTree.GetRootAsync(cancellationToken);
        SyntaxNode replacedRoot = currentRoot.RemoveNode(typeDecl, SyntaxRemoveOptions.KeepNoTrivia);

        document = document.WithSyntaxRoot(replacedRoot);

        document = await RemoveUnusedImportDirectivesAsync(document, cancellationToken);

        // create new tree for a new file
        // we drag all the usings because we don't know which are needed
        // and there is no easy way to find out which
        IEnumerable<SyntaxNode> currentUsings =
            currentRoot.DescendantNodesAndSelf().Where(s => s is UsingDirectiveSyntax);

        CompilationUnitSyntax newFileTree = SyntaxFactory.CompilationUnit()
            .WithUsings(SyntaxFactory.List(currentUsings.Select(i => (UsingDirectiveSyntax)i)))
            .WithMembers(SyntaxFactory.SingletonList<MemberDeclarationSyntax>(
                SyntaxFactory.NamespaceDeclaration(
                    SyntaxFactory.IdentifierName(typeSymbol.ContainingNamespace.ToString()))))
            .WithoutLeadingTrivia()
            .NormalizeWhitespace();

        newFileTree = newFileTree.WithMembers(SyntaxFactory.List(newFileTree.Members.Select(m =>
        {
            if (m is NamespaceDeclarationSyntax)
                return ((NamespaceDeclarationSyntax)m).WithMembers(
                    SyntaxFactory.SingletonList<MemberDeclarationSyntax>(typeDecl));

            return m;
        })));

        //move to new File
        //TODO: handle name conflicts
        Document newDocument = document.Project.AddDocument($"{identifierToken.Text}.cs",
            SourceText.From(newFileTree.ToFullString()),
            document.Folders);

        newDocument = await RemoveUnusedImportDirectivesAsync(newDocument, cancellationToken);

        return newDocument.Project.Solution;
    }

    public static async Task<Document> SortImportDirectivesAsync(this Document document,
                                                                 CancellationToken cancellationToken = default)
    {
        SyntaxNode root = await document.GetSyntaxRootAsync(cancellationToken);
        SyntaxTriviaList leadingTrivia = root.GetLeadingTrivia();
        var rootStx = (CompilationUnitSyntax)root;
        root = rootStx.WithUsings(Sort(rootStx.Usings)).WithLeadingTrivia(leadingTrivia);
        document = document.WithSyntaxRoot(root);

        return document;
    }

    public static async Task<Document> RemoveUnusedImportDirectivesAsync(
        this Document document,
        CancellationToken cancellationToken = default)
    {
        SyntaxNode root = await document.GetSyntaxRootAsync(cancellationToken);
        SemanticModel semanticModel = await document.GetSemanticModelAsync(cancellationToken);

        root = RemoveUnusedImportDirectives(semanticModel, root, cancellationToken);
        document = document.WithSyntaxRoot(root);

        return document;
    }

    private static SyntaxNode RemoveUnusedImportDirectives(SemanticModel semanticModel,
                                                           SyntaxNode root,
                                                           CancellationToken cancellationToken)
    {
        var oldUsings = root.DescendantNodesAndSelf().OfType<UsingDirectiveSyntax>().ToList();
        HashSet<SyntaxNode> unusedUsings = GetUnusedImportDirectives(semanticModel, cancellationToken);
        SyntaxTriviaList leadingTrivia = root.GetLeadingTrivia();

        root = root.RemoveNodes(oldUsings, SyntaxRemoveOptions.KeepNoTrivia);

        SyntaxList<UsingDirectiveSyntax> newUsings =
            SyntaxFactory.List(oldUsings.Except(unusedUsings).Cast<UsingDirectiveSyntax>());

        SyntaxList<UsingDirectiveSyntax> sortedUsings = Sort(newUsings);
        var compilation = (CompilationUnitSyntax)root;
        root = compilation!.WithUsings(sortedUsings).WithLeadingTrivia(leadingTrivia);

        return root;
    }

    private static HashSet<SyntaxNode> GetUnusedImportDirectives(SemanticModel model,
                                                                 CancellationToken cancellationToken)
    {
        var unusedImportDirectives = new HashSet<SyntaxNode>();
        SyntaxNode root = model.SyntaxTree.GetRoot(cancellationToken);

        foreach (Diagnostic diagnostic in model.GetDiagnostics(null, cancellationToken)
                     .Where(d => d.Id is "CS8019" or "CS0105"))
        {
            var usingDirectiveSyntax = root.FindNode(diagnostic.Location.SourceSpan) as UsingDirectiveSyntax;

            if (usingDirectiveSyntax != null)
                unusedImportDirectives.Add(usingDirectiveSyntax);
        }

        return unusedImportDirectives;
    }

    private static SyntaxList<UsingDirectiveSyntax> Sort(SyntaxList<UsingDirectiveSyntax> usingDirectives,
                                                         bool placeSystemNamespaceFirst = false)
    {
        IOrderedEnumerable<UsingDirectiveSyntax> oreredUsings = usingDirectives
            .OrderBy(x => x.StaticKeyword.IsKind(SyntaxKind.StaticKeyword) ? 1 : x.Alias == null ? 0 : 2)
            .ThenBy(x => x.Alias?.ToString())
            .ThenByDescending(x => placeSystemNamespaceFirst && x.Name!.ToString().StartsWith(nameof(System) + "."))
            .ThenBy(x => x.Name!.ToString());

        return SyntaxFactory.List(oreredUsings);
    }
}