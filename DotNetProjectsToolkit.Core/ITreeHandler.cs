﻿using FlakEssentials.MSBuild;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DotNetProjectsToolkit.Core;

public interface ITreeHandler
{
    Task OpenSolutionAsync(MSSolution solution);

    Task BuildFilesTreeAsync(IList<MSProjectItem> projectFilesPaths,
                             MSSolution solution,
                             MSProject project,
                             bool isSingleProject = true);
    T GetTree<T>() where T : class;
}