﻿namespace DotNetProjectsToolkit.Core;

public enum OperationMode
{
    XamlLocalization,
    ProjectFilesDiff
}