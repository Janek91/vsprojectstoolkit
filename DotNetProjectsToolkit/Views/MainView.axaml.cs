﻿using DotNetProjectsToolkit.ViewModels;
using FEx.Avaloniax.Abstractions;

namespace DotNetProjectsToolkit.Views;

public partial class MainView : FExAvaloniaReactiveUserControl<MainViewModel>
{
    public MainView()
    {
        InitializeComponent();
    }
}