﻿using FEx.Avaloniax.Abstractions;
using FEx.Avaloniax.Abstractions.Interfaces;

namespace DotNetProjectsToolkit.ViewModels;

public class MainViewModel : FExAvaloniaViewModelBase
{
    public MainViewModel(INavigationService navigationService)
        : base(navigationService)
    {
    }

    public MainViewModel()
        : base(null)
    {
    }
}