﻿using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using DotNetProjectsToolkit.ViewModels;
using DotNetProjectsToolkit.Views;
using FEx.Avaloniax;

namespace DotNetProjectsToolkit;

public class App : FExAvaloniaApp<DotNetProjectsToolkitContainer>
{
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override void OnFrameworkInitializationCompleted()
    {
        switch (ApplicationLifetime)
        {
            case IClassicDesktopStyleApplicationLifetime desktop:
                desktop.MainWindow = new MainWindow
                {
                    DataContext = new MainViewModel()
                };

                break;
            case ISingleViewApplicationLifetime singleViewPlatform:
                singleViewPlatform.MainView = new MainView
                {
                    DataContext = new MainViewModel()
                };

                break;
        }

        base.OnFrameworkInitializationCompleted();
    }
}