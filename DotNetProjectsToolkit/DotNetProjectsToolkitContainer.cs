using FEx.Avaloniax;
using FEx.Avaloniax.Abstractions.Interfaces;
using FEx.Fundamentals;
using FEx.Json;
using FEx.Logging;
using FEx.MVVM;
using FEx.MVVM.Rx;
using FEx.Platforms;
using StrongInject;

namespace DotNetProjectsToolkit;

[RegisterModule(typeof(FExLoggingModule))]
[RegisterModule(typeof(FExJsonModule))]
[RegisterModule(typeof(FExMvvmModule))]
[RegisterModule(typeof(FExMvvmRxModule))]
[RegisterModule(typeof(FExPlatformsModule))]
[RegisterModule(typeof(FExAvaloniaxModule))]
public partial class DotNetProjectsToolkitContainer : FExFundamentalsModule, IFExContainer 
{
}