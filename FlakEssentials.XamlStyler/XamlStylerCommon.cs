﻿using FEx.Json.Extensions;
using FlakEssentials.Extensions.Utilities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace FlakEssentials.XamlStyler
{
    /// <summary>
    /// Class XamlStylerCommon.
    /// </summary>
    public class XamlStylerCommon
    {
        public enum LogLevel
        {
            None,
            Minimal,
            Default,
            Verbose,
            Debug,
            Insanity
        }

        public static FileInfo XStyler { get; set; }

        /// <summary>
        /// Gets the default settings string.
        /// </summary>
        /// <returns>System.String.</returns>
        public static string GetDefaultSettingsString()
        {
            string res = null;
            var assembly = Assembly.LoadFile("xstyler.exe");
            string[] names = assembly.GetManifestResourceNames();
            string resourceName = names.FirstOrDefault(x => x.Contains(".DefaultSettings.json"));

            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream != null)
                    using (var reader = new StreamReader(stream))
                        res = reader.ReadToEnd();
            }

            return res;
        }

        /// <summary>
        /// Gets the default settings.
        /// </summary>
        /// <returns>FlakDynamicObject.</returns>
        public FlakDynamicObject GetDefaultSettings() => GetDefaultSettingsString().FromJson<FlakDynamicObject>();

        /// <summary>
        /// Reformats the xaml file.
        /// </summary>
        /// <param name="settings">The settings.</param>
        /// <param name="filePath">The file path.</param>
        /// <param name="logLevel">The log level.</param>
        /// <returns>System.String.</returns>
        public string ReformatXamlFile(FlakDynamicObject settings,
                                       string filePath,
                                       LogLevel logLevel = LogLevel.Default)
        {
            string settingsJsonFilePath = Path.Combine(Path.GetTempPath(), "DefaultSettings.json");
            File.WriteAllText(settingsJsonFilePath, settings.ToJson());

            return ReformatXamlFile([filePath], settingsJsonFilePath, logLevel);
        }

        /// <summary>
        /// Reformats the xaml file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="settingsJsonFilePath">The settings json file path.</param>
        /// <param name="logLevel">The log level.</param>
        /// <returns>System.String.</returns>
        public static string ReformatXamlFile(string filePath,
                                              string settingsJsonFilePath = null,
                                              LogLevel logLevel = LogLevel.Default) =>
            ReformatXamlFile([filePath], settingsJsonFilePath, logLevel);

        /// <summary>
        /// Reformats the xaml file.
        /// </summary>
        /// <param name="filePaths">The file paths.</param>
        /// <param name="settingsJsonFilePath">The settings json file path.</param>
        /// <param name="logLevel">The log level.</param>
        /// <returns>System.String.</returns>
        public static string ReformatXamlFile(IEnumerable<string> filePaths,
                                              string settingsJsonFilePath = null,
                                              LogLevel logLevel = LogLevel.Default)
        {
            var sb = new StringBuilder();

            foreach (string filePath in filePaths)
            {
                var args = new List<string>
                {
                    "-f",
                    filePath,
                    "-l",
                    logLevel.ToString()
                };

                if (settingsJsonFilePath != null)
                    args.AddRange(["-c", settingsJsonFilePath]);

                using (var cmd = new Cmd())
                {
                    var arg = $"dotnet \"{XStyler.FullName}\" {string.Join(" ", args)}";
                    cmd.Run(arg);

                    if (cmd.Output.Length > 0)
                        sb.AppendLine(cmd.Output.ToString());

                    if (cmd.ErrOut.Length > 0)
                        sb.AppendLine(cmd.ErrOut.ToString());
                }
            }

            return sb.ToString();
        }
    }
}