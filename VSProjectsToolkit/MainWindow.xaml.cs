using DotNetProjectsToolkit.Core;
using FEx.Abstractions;
using FEx.Logging;
using FEx.WPFx.Dialogs;
using FlakEssentials.MSBuild;
using FlakEssentials.TelerikEx.Controls;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Telerik.Windows;
using Telerik.Windows.Controls;

namespace VSProjectsToolkit;

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : FlakTelerikWindow<MainViewModel>
{
    public MainWindow()
    {
        SetEnglishCultureInfo();
        InitializeComponent();
        ViewModel!.XamlContent = string.Empty;
        //PrepareEditor();
    }

    /// <summary>
    /// Sets the culture information to English.
    /// </summary>
    public static void SetEnglishCultureInfo()
    {
        var culture = new CultureInfo("en-US")
        {
            DateTimeFormat =
            {
                FirstDayOfWeek = DayOfWeek.Monday
            }
        };

        Thread.CurrentThread.CurrentCulture = culture;
        Thread.CurrentThread.CurrentUICulture = culture;
    }

    private void MenuItem_OnClick(object sender, RoutedEventArgs e) => Start();

    private async void Start()
    {
        await ViewModel!.PrepareXamlStylerConsoleAsync();

        var dialogOptions = new OpenFileDialogOptions
        {
            Title = "Choose desired VS project or solution file:",
            Filter = "Projects and solutions files | *.csproj; *.sln"
        };

        if (dialogOptions.ShowDialog(this))
            await RunTaskAsync(() => ViewModel!.StartAsync(dialogOptions.FileName));
    }

    private void MainWindow_OnContentRendered(object sender, EventArgs e)
    {
        Start();
    }

    private async void MenuItemExport_OnClick(object sender, RoutedEventArgs e) =>
        await RunTaskAsync(() => ViewModel!.ExportAsync());

    private async void DataGrid_OnSelectionChanged(object sender, SelectionChangeEventArgs e)
    {
        var grid = (RadGridView)sender;
        ViewModel.SelectedItems.Clear();
        ViewModel.SelectedItems.AddRange(grid.SelectedItems.OfType<XamlString>());
        await RunAsync(() => ViewModel.ShowXaml());
    }

    //private void PrepareEditor()
    //{
    //    // Reset the styles
    //    _scintilla.StyleResetDefault();
    //    _scintilla.Styles[ScintillaNET.Style.Default].Font = "Consolas";
    //    _scintilla.Styles[ScintillaNET.Style.Default].Size = 10;
    //    _scintilla.StyleClearAll();

    //    // Set the XML Lexer
    //    _scintilla.Lexer = Lexer.Xml;

    //    // Show line numbers
    //    _scintilla.Margins[0].Width = 20;

    //    // Enable folding
    //    _scintilla.SetProperty("fold", "1");
    //    _scintilla.SetProperty("fold.compact", "1");
    //    _scintilla.SetProperty("fold.html", "1");

    //    // Use Margin 2 for fold markers
    //    _scintilla.Margins[2].Type = MarginType.Symbol;
    //    _scintilla.Margins[2].Mask = Marker.MaskFolders;
    //    _scintilla.Margins[2].Sensitive = true;
    //    _scintilla.Margins[2].Width = 20;

    //    // Reset folder markers
    //    for (int i = Marker.FolderEnd; i <= Marker.FolderOpen; i++)
    //    {
    //        _scintilla.Markers[i].SetForeColor(SystemColors.ControlLightLight);
    //        _scintilla.Markers[i].SetBackColor(SystemColors.ControlDark);
    //    }

    //    // Style the folder markers
    //    _scintilla.Markers[Marker.Folder].Symbol = MarkerSymbol.BoxPlus;
    //    _scintilla.Markers[Marker.Folder].SetBackColor(SystemColors.ControlText);
    //    _scintilla.Markers[Marker.FolderOpen].Symbol = MarkerSymbol.BoxMinus;
    //    _scintilla.Markers[Marker.FolderEnd].Symbol = MarkerSymbol.BoxPlusConnected;
    //    _scintilla.Markers[Marker.FolderEnd].SetBackColor(SystemColors.ControlText);
    //    _scintilla.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
    //    _scintilla.Markers[Marker.FolderOpenMid].Symbol = MarkerSymbol.BoxMinusConnected;
    //    _scintilla.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
    //    _scintilla.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;

    //    // Enable automatic folding
    //    _scintilla.AutomaticFold = AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change;

    //    // Set the Styles
    //    _scintilla.StyleResetDefault();
    //    // I like fixed font for XML
    //    _scintilla.Styles[ScintillaNET.Style.Default].Font = "Courier";
    //    _scintilla.Styles[ScintillaNET.Style.Default].Size = 10;
    //    _scintilla.StyleClearAll();
    //    _scintilla.Styles[ScintillaNET.Style.Xml.Attribute].ForeColor = Color.Red;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.Entity].ForeColor = Color.Red;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.Comment].ForeColor = Color.Green;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.Tag].ForeColor = Color.Blue;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.TagEnd].ForeColor = Color.Blue;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.DoubleString].ForeColor = Color.DeepPink;
    //    _scintilla.Styles[ScintillaNET.Style.Xml.SingleString].ForeColor = Color.DeepPink;
    //}

    private async void MenuItemReformat_OnClick(object sender, RoutedEventArgs e) =>
        await RunTaskAsync(() => ViewModel!.ReformatAllXamlsAsync());

    private void MenuItemOpenLog_OnClick(object sender, RadRoutedEventArgs e) => FExLogging.OpenLogFile();

    private async void MenuItemRebind_OnClick(object sender, RadRoutedEventArgs radRoutedEventArgs) =>
        await RunAsync(() => ViewModel!.Rebind());

    private async void ButtonExcel_OnClick(object sender, RoutedEventArgs e) => await ViewModel!.ExportToExcelAsync();

    private async void ButtonBase_OnClick(object sender, RoutedEventArgs e) =>
        await RunAsync(() => ViewModel!.AddMask());

    private void DiffDataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
    {
        var value = diffDataGrid.CurrentCell?.Value?.ToString();

        if (value != null)
        {
            Clipboard.SetText(value);
            CreateAlert(FExFoundation.AppInfoProvider.Name, "Copied to clipboard");
        }
    }

    private async void DeleteButton_OnClick(object sender, RoutedEventArgs e) =>
        await RunAsync(() => ViewModel!.DeleteSelectedFiles(diffDataGrid.SelectedItems.OfType<DiffResult>().ToList()),
            sender);

    private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
    {
        Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri)
            {
                UseShellExecute = true
            })
            ?.Dispose();

        e.Handled = true;
    }

    private async void MenuItemReformatCs_OnClick(object sender, RadRoutedEventArgs e) =>
        await RunTaskAsync(() => ViewModel!.ReformatAllCsFilesAsync(), sender);
}