using DotNetProjectsToolkit.Core;
using FEx.Abstractions.Interfaces;
using FEx.Basics.Collections.Concurrent;
using FEx.Extensions.Collections.Enumerables;
using FEx.MVVM.Abstractions;
using FEx.WPFx.Abstractions.Interfaces;
using FlakEssentials.MSBuild;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Windows.Controls;

namespace VSProjectsToolkit;

public class TreeHandler : ITreeHandler
{
    private readonly ITreeViewBuilder<RadTreeViewItem> _treeBuilder;
    private readonly IFExDispatcher _dispatcher;

    public ConcurrentObservableList<RadTreeViewItem> TreeViewItemsSource { get; }

    public TreeHandler(ITreeViewBuilder<RadTreeViewItem> treeBuilder, IFExDispatcher dispatcher)
    {
        _treeBuilder = treeBuilder;
        _dispatcher = dispatcher;
        TreeViewItemsSource = [];
    }

    public async Task OpenSolutionAsync(MSSolution solution)
    {
        TreeViewItemsSource.Clear();

        _treeBuilder.CreateNewNode(
            new FExTreeViewNode(solution.Name, null, '\\', solution.SolutionFilePath, true, true));

        await _dispatcher.InvokeOnMainThreadAsync(() => AddTreeAsync(solution));
    }

    public async Task BuildFilesTreeAsync(IList<MSProjectItem> projectFilesPaths,
                                          MSSolution solution,
                                          MSProject project,
                                          bool isSingleProject = true)
    {
        if (isSingleProject)
            TreeViewItemsSource.Clear();

        _treeBuilder.CreateNewNode(new FExTreeViewNode(project.ProjectFile, null, '\\', project.FullPath));

        _treeBuilder.AddChildNodes(project.ProjectFile,
            projectFilesPaths.Select(curr => new FExTreeViewNode(curr.EvaluatedInclude, null, '\\', curr.FullPath))
                .ToList());

        RadTreeViewItem tree = await _treeBuilder.GetTreeNodeAsync(project.ProjectFile, true);

        if (isSingleProject || solution is null)
            TreeViewItemsSource.Add(tree);
        else
            await _dispatcher.InvokeOnMainThreadAsync(() =>
            {
                int idx = TreeViewItemsSource.IndexWhere(x =>
                    x.Name == FExTreeViewNode.FixTreeViewItemName(solution.Name));

                TreeViewItemsSource[idx].Items.Add(tree);
            });
    }

    public T GetTree<T>() where T : class => TreeViewItemsSource as T;

    private async Task AddTreeAsync(MSSolution solution)
    {
        RadTreeViewItem tree = await _treeBuilder.GetTreeNodeAsync(solution.Name, true);
        TreeViewItemsSource.Add(tree);
    }
}