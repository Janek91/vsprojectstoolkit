using DotNetProjectsToolkit.Core;
using FEx.Basics.Collections.Concurrent;
using FEx.DI.Abstractions;
using FEx.Extensions;
using FEx.WPFx.Dialogs;
using FlakEssentials.MSBuild;
using FlakEssentials.TelerikEx.Enums;
using FlakEssentials.TelerikEx.ViewModels;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Controls;

namespace VSProjectsToolkit;

public class MainViewModel : TelerikViewModelBase
{
    private string _xamlContent;
    private bool _dataGridIsBusy;

    public new MainWindow View
    {
        get => (MainWindow)base.View;
        set => base.View = value;
    }

    public string XamlContent
    {
        get => _xamlContent;
        set => SetProperty(ref _xamlContent, value);
    }

    public bool DataGridIsBusy
    {
        get => _dataGridIsBusy;
        set => SetProperty(ref _dataGridIsBusy, value);
    }

    public ConcurrentObservableList<TelerikTheme> AvailableThemes { get; }

    public IProjectsService ProjectsService { get; }
    public ConcurrentObservableList<XamlString> SelectedItems { get; }

    public ConcurrentObservableList<RadTreeViewItem> TreeViewItemsSource { get; }

    public MainViewModel()
    {
        SelectedItems = [];

        ProjectsService = FExServiceProvider.Get<IProjectsService>();
        ProjectsService.XamlContentChanged += (_, xamlContent) => OnXamlContentChanged(xamlContent);
        ProjectsService.DataLoaded += (_, _) => OnDataLoaded();
        SubscribeToProgressExcept(ProjectsService);
        TreeViewItemsSource = ProjectsService.TreeHandler.GetTree<ConcurrentObservableList<RadTreeViewItem>>();
    }

    public async Task StartAsync(string fileName) => await ProjectsService.StartAsync(fileName);

    public async Task ExportToExcelAsync(string extension = "xls")
    {
        var saveFileDialogOptions = new SaveFileDialogOptions
        {
            Title = "Where save exported excel file?",
            Filter = $"Excel files (*.{extension})|*.{extension}|All files (*.*)|*.*",
            DefaultExt = extension
        };

        {
            if (saveFileDialogOptions.ShowDialog(View, Progress))
            {
                await RunAsync(() =>
                {
                    Dispatcher.InvokeOnMainThread(() =>
                    {
                        using Stream stream = saveFileDialogOptions.Dialog.OpenFile();

                        View.diffDataGrid.Export(stream,
                            new()
                            {
                                Format = ExportFormat.ExcelML,
                                ShowColumnHeaders = true,
                                ShowColumnFooters = true,
                                ShowGroupFooters = false,
                                Culture = CultureInfo.CurrentCulture,
                                Encoding = Encoding.UTF8
                            });
                    });
                });

                Process.Start(saveFileDialogOptions.Dialog.FileName)?.Dispose();
            }
        }
    }

    public async Task PrepareXamlStylerConsoleAsync() => await ProjectsService.PrepareXamlStylerConsoleAsync();

    public async Task ExportAsync() => await ProjectsService.ExportAsync();

    public void ShowXaml() => ProjectsService.ShowXaml(SelectedItems.FirstOrDefault());

    public async Task<string[]> ReformatAllXamlsAsync() => await ProjectsService.ReformatAllXamlsAsync();

    public void AddMask() => ProjectsService.AddMask();

    public void Rebind() => ProjectsService.Rebind();

    public void DeleteSelectedFiles(List<DiffResult> selectedResults) =>
        ProjectsService.DeleteSelectedFiles(selectedResults);

    public async Task<bool> ReformatAllCsFilesAsync() => await ProjectsService.ReformatAllCsFilesAsync();

    private void OnXamlContentChanged(string xamlContent)
    {
        Dispatcher.InvokeOnMainThread(() =>
        {
            XamlContent = xamlContent;

            if (View?.xamlPreviewPanel == null)
                return;

            if (xamlContent.IsNullOrWhiteSpace())
            {
                View.xamlPreviewPanel.Visibility = Visibility.Collapsed;
                View.gridSplitter.Visibility = Visibility.Collapsed;
            }
            else if (View.xamlPreviewPanel.Visibility != Visibility.Visible)
            {
                View.gridSplitter.Visibility = Visibility.Visible;
                View.xamlPreviewPanel.Visibility = Visibility.Visible;
                //View.xamlFilesColumn.MinWidth = View.dataGrid.DesiredSize.Width / 2;
            }
        });
    }

    private void OnDataLoaded()
    {
        Dispatcher.InvokeOnMainThread(() =>
        {
            ProjectsService.DataGridIsBusy = false;

            if (View.dataGrid.Items.Count > 0)
            {
                View.dataGrid.SelectedItem = View.dataGrid.Items[0];
                View.dataGrid.ScrollIntoView(View.dataGrid.Items[0]);
            }
        });
    }
}